package br.com.itau.tarefas.sample.caller.service.service;

import br.com.itau.tarefas.sample.caller.service.model.SampleModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "sample-service")
public interface SampleService {

    @GetMapping("/")
    SampleModel getModel();
}
