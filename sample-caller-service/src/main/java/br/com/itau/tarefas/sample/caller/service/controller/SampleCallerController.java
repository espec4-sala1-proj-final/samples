package br.com.itau.tarefas.sample.caller.service.controller;

import br.com.itau.tarefas.sample.caller.service.model.LogModel;
import br.com.itau.tarefas.sample.caller.service.model.SampleModel;
import br.com.itau.tarefas.sample.caller.service.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;

@RestController
public class SampleCallerController {

    @Autowired
    private SampleService sampleService;

    @GetMapping("/")
    public SampleModel sample(){
        return new SampleModel("Caller up and running");
    }

    @GetMapping("/call")
    public SampleModel call(){
        return new SampleModel("Calling sample-service. Response: "+sampleService.getModel().getMessage());
    }

    @GetMapping("/log/{code}/{message}")
    public ResponseEntity<Object> logGet(@PathVariable String code, @PathVariable String message){
        int statusCode = Integer.parseInt(code);
        HttpStatus httpStatus = Arrays.stream(HttpStatus.values())
                .filter(status -> status.value() == statusCode)
                .findAny()
                .orElse(HttpStatus.OK);
        System.out.println(message);
        return ResponseEntity.status(httpStatus).body(new LogModel(statusCode, message));
    }

    @PostMapping("/log/{code}")
    public ResponseEntity<Object> logPost(@PathVariable String code, @RequestBody SampleModel sampleModel){
        int statusCode = Integer.parseInt(code);
        HttpStatus httpStatus = Arrays.stream(HttpStatus.values())
                .filter(status -> status.value() == statusCode)
                .findAny()
                .orElse(HttpStatus.OK);
        System.out.println(sampleModel.getMessage());
        return ResponseEntity.status(httpStatus).body(new LogModel(statusCode, sampleModel.getMessage()));
    }
}
