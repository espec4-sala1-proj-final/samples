package br.com.itau.tarefas.sample.caller.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SampleCallerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleCallerServiceApplication.class, args);
	}

}
