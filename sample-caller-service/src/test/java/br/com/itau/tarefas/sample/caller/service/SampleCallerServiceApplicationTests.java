package br.com.itau.tarefas.sample.caller.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SampleCallerServiceApplicationTests {

	@Test
	public void contextLoads() {
	}

}
