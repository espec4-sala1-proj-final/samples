package br.com.itau.tarifas.sample.service.model;

public class SampleModel {
    private String message;

    public SampleModel(){}

    public SampleModel(String message){
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
