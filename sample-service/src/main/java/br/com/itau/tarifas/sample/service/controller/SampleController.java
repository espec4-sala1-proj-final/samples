package br.com.itau.tarifas.sample.service.controller;

import br.com.itau.tarifas.sample.service.model.SampleModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleController {

    @GetMapping("/")
    public SampleModel getModel(){
        return new SampleModel("up and running!");
    }
}
